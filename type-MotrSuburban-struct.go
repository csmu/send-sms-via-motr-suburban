package main

// MotrSuburban is used to create the payload used to send a message via motr.com.au and to store the results
type MotrSuburban struct {
	Link     string `json:"link"`
	Username string `json:"username"`
	Password string `json:"password"`
	Mobile   string `json:"mobile"`
	Message  string `json:"message"`
	Results  []byte `json:"results"`
}
