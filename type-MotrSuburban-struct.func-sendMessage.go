package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

func (motrSuburban *MotrSuburban) sendMessage() error {

	var err error
	var httpMethod = "POST"
	var soapAction string
	var payload []byte
	var request *http.Request
	var transport *http.Transport
	var client *http.Client
	var response *http.Response

	if motrSuburban.Link == "" {
		motrSuburban.Link = "http://motr.com.au/server.php?wsdl"
	}

	soapAction = "urn:suburbanwebservice"
	payload = []byte(fmt.Sprintf(strings.TrimSpace(`
	<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
    <Body>
        <sendMessage xmlns="urn:suburbanwebservice">
            <Username>%s</Username>
            <Password>%s</Password>
            <Mobile>%s</Mobile>
            <Message>%s</Message>
        </sendMessage>
    </Body>
</Envelope>`), motrSuburban.Username, motrSuburban.Password, motrSuburban.Mobile, motrSuburban.Message))
	request, err = http.NewRequest(httpMethod, motrSuburban.Link, bytes.NewReader(payload))
	if err == nil {
		request.Header.Set("Content-type", "text/xml")
		request.Header.Set("SOAPAction", soapAction)
		transport = &http.Transport{}
		client = &http.Client{Transport: transport}
		response, err = client.Do(request)
		if err == nil {
			if response.Body != nil {
				defer response.Body.Close()
				motrSuburban.Results, err = ioutil.ReadAll(response.Body)
			}
		}
	}
	return err
}
