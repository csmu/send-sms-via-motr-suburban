APP=send-sms-via-motr-suburban

build: clean
	go build -o ${APP} *.go

run:
	go run -race *.go
	
clean:
	go clean
